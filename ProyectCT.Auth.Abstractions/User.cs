﻿using System;
using System.Collections.Generic;
using ProyectCT.Auth.Abstractions;
using System.ComponentModel.DataAnnotations;
using ProyectCT.Auth.Abstractions.Resources;
using ProyectCT.Common;
using ProyectCT.Common.Resources;

namespace ProyectCT.Auth
{
    public class User : IMemberwiseComparable<User>
    {
        /// <summary>
        /// Unique identifier for the user.
        /// </summary>
        [Display(
            Name = nameof(Captions.UserId),
            ShortName = nameof(Captions.IdShortName),
            ResourceType = typeof(Captions))]
        [Required(
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string UserId { get; set; }

        /// <summary>
        /// First name of the user.
        /// </summary>
        [Display(
            Name = nameof(Captions.FirstName),
            ResourceType = typeof(Captions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.RegularNameMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the user.
        /// </summary>
        [Display(
            Name = nameof(Captions.LastName),
            ResourceType = typeof(Captions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.RegularNameMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string LastName { get; set; }

        /// <summary>
        /// E-mail address of the user.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.Email),
            ResourceType = typeof(CommonCaptions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.EmailMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string Email { get; set; }

        /// <summary>
        /// Hashed and salted version of the user's password
        /// </summary>
        [Display(
            Name = nameof(Captions.PasswordHash),
            ResourceType = typeof(Captions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.PasswordHashMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string PasswordHash { get; set; }

        /// <summary>
        /// URL address of the profile photo of the user.
        /// </summary>
        [Display(
            Name = nameof(Captions.ProfilePhoto),
            ShortName = nameof(Captions.Photo),
            ResourceType = typeof(Captions))]
        [MaxLength(
            StandardLimits.UrlMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string ProfilePhotoUrl { get; set; }

        /// <summary>
        /// URL address of a small thumbnail version of the profile photo of the user.
        /// </summary>
        [Display(
            Name = nameof(Captions.ProfilePhotoSmall),
            ShortName = nameof(Captions.Photo),
            ResourceType = typeof(Captions))]
        [MaxLength(
            StandardLimits.UrlMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string ProfilePhotoSmallUrl { get; set; }

        /// <summary>
        /// Id of CurrentPlan
        /// </summary>
        [Display(
            Name = nameof(Captions.GoogleUid),
            ShortName = nameof(Captions.IdShortName),
            ResourceType = typeof(Captions))]
        [MaxLength(
            StandardLimits.UuidMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string GoogleUid { get; set; }

        /// <summary>
        /// Gets access to audit trail details for this entity.
        /// </summary>
        public AuditInfo AuditInfo { get; set; } = new AuditInfo();

        public List<MemberComparison> CompareMembers(User other)
        {
            return MemberComparer<User>.Compare(other, this)
             .Property(p => p.UserId)
             .Property(p => p.FirstName)
             .Property(p => p.LastName)
             .Property(p => p.Email)
             .Property(p => p.PasswordHash)
             .Property(p => p.ProfilePhotoUrl)
             .Property(p => p.ProfilePhotoSmallUrl)
             .Property(p => p.GoogleUid)
             .GetDifferences();
        }

        public bool Equals(User other)
        {
            return MemberComparer<User>.CheckValueEquality(other, this)
             .Property(p => p.UserId)
             .Property(p => p.FirstName)
             .Property(p => p.LastName)
             .Property(p => p.Email)
             .Property(p => p.PasswordHash)
             .Property(p => p.ProfilePhotoUrl)
             .Property(p => p.ProfilePhotoSmallUrl)
             .Property(p => p.GoogleUid)
             .AreEqual();
        }

        public override int GetHashCode()
        {
            return new HashCodeCalculator<User>(this)
             .Property(p => p.UserId)
             .Property(p => p.FirstName)
             .Property(p => p.LastName)
             .Property(p => p.Email)
             .Property(p => p.PasswordHash)
             .Property(p => p.ProfilePhotoUrl)
             .Property(p => p.ProfilePhotoSmallUrl)
             .Property(p => p.GoogleUid)
             .GetFinalHash();
        }

        public User Clone() => new User
        {
            UserId = UserId,
            FirstName = FirstName,
            LastName = LastName,
            Email = Email,
            PasswordHash = PasswordHash,
            ProfilePhotoUrl = ProfilePhotoUrl,
            ProfilePhotoSmallUrl = ProfilePhotoSmallUrl,
            GoogleUid = GoogleUid,
            AuditInfo = AuditInfo.Clone(),
        };

        public override string ToString() => $"User: {(FirstName + " " + LastName).Trim()} ({UserId})";
    }
}
