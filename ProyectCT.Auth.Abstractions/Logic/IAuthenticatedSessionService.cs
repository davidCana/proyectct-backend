﻿using System;

namespace ProyectCT.Auth.Logic
{
    public interface IAuthenticatedSessionService
    {
        Guid GetCurrentSessionUserId();
        string GetCurrentSessionUserName();
    }
}
