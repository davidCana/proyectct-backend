﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Auth.Logic
{
    public interface IUserManager
    {
        Task AddAsync(User user);
        Task UpdateAsync(User user);
        Task DeactivateAsync(string userId);
        Task<User> GetByIdAsync(string userId);
        Task<IEnumerable<User>> ListByIdsAsync(IEnumerable<string> userIds);
    }
}
