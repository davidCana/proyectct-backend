﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Auth.DataAccess
{
    public interface IUserRepository
    {
        Task InsertAsync(User user);
        Task UpdateAsync(User user);
        Task SoftDeleteAsync(string userId);
        Task HardDeleteAsync(string userId);
        Task<User> GetByIdAsync(string userId);
        Task<IEnumerable<User>> ListByIdsAsync(IEnumerable<string> userIds);
    }
}
