﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProyectCT.Core.Abstractions.DataAccess;

namespace ProyectCT.Core.DataAccess
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection RegisterProyectCTEfDataAccess(this IServiceCollection services, IConfiguration configuration)
        {
            ////Add EF DbContexts
            //services.AddDbContext<ProyectCTMainDbContext>(options =>
            //{
            //    options.UseSqlServer(configuration.GetConnectionString("IdentityConnection"));
            //}, contextLifetime: ServiceLifetime.Transient);


            services.AddDbContext<ProyectCTMainDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("IdentityConnection"))
            );

            //Add implemented repository classes
            services.AddTransient<IPersonalInformationRepository, EfPersonalInformationRepository>();
            services.AddTransient<IEmployerRepository, EfEmployerRepository>();
            services.AddTransient<IEducationLevelRepository, EfEducationLevelRepository>();

            return services;
        }
    }
}

