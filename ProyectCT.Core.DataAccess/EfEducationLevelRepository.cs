﻿using Microsoft.EntityFrameworkCore;
using ProyectCT.Auth.Logic;
using ProyectCT.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public class EfEducationLevelRepository : IEducationLevelRepository
    {
        private readonly ProyectCTMainDbContext _dbContext;


        public EfEducationLevelRepository(ProyectCTMainDbContext dbContext)
        {
            _dbContext = dbContext;
          
        }

        public async Task<EducationLevel> GetByIdAsync(int trainingID)
        {
            return await _dbContext.EducationLevel.FindAsync(trainingID);
        }

        public async Task HardDeleteAsync(int EducationLevelID)
        {
            var EducationLevel = await _dbContext.EducationLevel.Where(u => u.EducationLevelID == EducationLevelID).SingleAsync();

            _dbContext.EducationLevel.Remove(EducationLevel);
            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(EducationLevel EducationLevel)
        {
            await _dbContext.EducationLevel.AddAsync(EducationLevel);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<EducationLevel>> ListAllAsync(string UserID)
        {
            return await _dbContext.EducationLevel.Where(d => d.UserID == UserID).ToListAsync();
        }

        public async Task SoftDeleteAsync(int EducationLevelID)
        {
            var EducationLevel = await _dbContext.EducationLevel.Where(u => u.EducationLevelID == EducationLevelID).SingleAsync();

            _dbContext.EducationLevel.Update(EducationLevel);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(EducationLevel EducationLevel)
        {
            var EducationLevelOriginal = await _dbContext.EducationLevel.FindAsync(EducationLevel.EducationLevelID);

            EducationLevelOriginal = EducationLevel;

            _dbContext.EducationLevel.Update(EducationLevelOriginal);
            await _dbContext.SaveChangesAsync();
        }
    }
}
