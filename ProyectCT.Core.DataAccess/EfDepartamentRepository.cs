﻿using Microsoft.EntityFrameworkCore;
using ProyectCT.Auth.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public class EfDepartamentRepository : IDepartamentRepository
    {
        private readonly ProyectCTMainDbContext _dbContext;
        private readonly IAuthenticatedSessionService _sessionService;

        public EfDepartamentRepository(ProyectCTMainDbContext dbContext, IAuthenticatedSessionService sessionService)
        {
            _dbContext = dbContext;
            _sessionService = sessionService;
        }

        public async Task<Departament> GetByIdAsync(int DepartamentID)
        {
            return await _dbContext.Departament.FindAsync(DepartamentID);
        }

        public async Task HardDeleteAsync(int DepartamentID)
        {
            var Departament = await _dbContext.Departament.Where(u => u.DepartamentID == DepartamentID).SingleAsync();

            _dbContext.Departament.Remove(Departament);
            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(Departament Departament)
        {
            await _dbContext.Departament.AddAsync(Departament);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Departament>> ListAllAsync()
        {
            return await _dbContext.Departament.ToListAsync();
        }

        public async Task SoftDeleteAsync(int DepartamentID)
        {
            var Departament = await _dbContext.Departament.Where(u => u.DepartamentID == DepartamentID).SingleAsync();

            _dbContext.Departament.Update(Departament);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Departament Departament)
        {
            var DepartamentOriginal = await _dbContext.Departament.FindAsync(Departament.DepartamentID);

            DepartamentOriginal = Departament;

            _dbContext.Departament.Update(DepartamentOriginal);
            await _dbContext.SaveChangesAsync();
        }
    }
}
