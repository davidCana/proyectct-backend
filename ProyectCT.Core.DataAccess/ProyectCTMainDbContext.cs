﻿using Microsoft.EntityFrameworkCore;
using ProyectCT.Auth;
using ProyectCT.Common.EntityFramework;
using ProyectCT.Core.Abstractions;

namespace ProyectCT.Core.DataAccess
{
    public class ProyectCTMainDbContext : DbContext
    {
        public DbSet<WorkExperience> WorkExperience { get; set; }
        public DbSet<Training> Training { get; set; }
        public DbSet<PersonalInformation> PersonalInformation { get; set; }
        public DbSet<EducationLevel> EducationLevel { get; set; }
        public DbSet<Departament> Departament { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Employer> Employers { get; set; }

        public ProyectCTMainDbContext(DbContextOptions<ProyectCTMainDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().AddAuditInfoAsOwned();
            modelBuilder.Entity<WorkExperience>().AddAuditInfoAsOwned();
            modelBuilder.Entity<User>().AddAuditInfoAsOwned();
            modelBuilder.Entity<Training>().AddAuditInfoAsOwned();
            modelBuilder.Entity<PersonalInformation>();
        }
    }
}
