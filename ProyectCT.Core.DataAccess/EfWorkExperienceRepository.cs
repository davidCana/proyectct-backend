﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProyectCT.Auth.Logic;
using ProyectCT.Core.Abstractions;

namespace ProyectCT.Core.DataAccess
{
    class EfWorkExperienceRepository : IWorkExperienceRepository
    {
        private readonly ProyectCTMainDbContext _dbContext;
        private readonly IAuthenticatedSessionService _sessionService;

        public EfWorkExperienceRepository(ProyectCTMainDbContext dbContext, IAuthenticatedSessionService sessionService)
        {
            _dbContext = dbContext;
            _sessionService = sessionService;
        }

        public async Task<WorkExperience> GetByIdAsync(Guid WorkExperienceID)
        {
            return await _dbContext.WorkExperience.FindAsync(WorkExperienceID);
        }

        public async Task HardDeleteAsync(Guid WorkExperienceID)
        {
            var workExperience = await _dbContext.WorkExperience.Where(u => u.WorkExperienceID == WorkExperienceID).SingleAsync();

            _dbContext.WorkExperience.Remove(workExperience);
            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(WorkExperience workExperience)
        {
            workExperience.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());

            await _dbContext.WorkExperience.AddAsync(workExperience);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<WorkExperience>> ListAllAsync()
        {
            return await _dbContext.WorkExperience.ToListAsync();
        }

        public async Task SoftDeleteAsync(Guid WorkExperienceID)
        {
            var workExperience = await _dbContext.WorkExperience.Where(u => u.WorkExperienceID == WorkExperienceID).SingleAsync();

            workExperience.AuditInfo.MarkDeletion(_sessionService.GetCurrentSessionUserId());
            _dbContext.WorkExperience.Update(workExperience);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(WorkExperience WorkExperience)
        {
            var workExperienceOriginal = await _dbContext.WorkExperience.FindAsync(WorkExperience.WorkExperienceID);
            WorkExperience.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());
            workExperienceOriginal = WorkExperience;

            _dbContext.WorkExperience.Update(workExperienceOriginal);
            await _dbContext.SaveChangesAsync();

        }

    }
}
