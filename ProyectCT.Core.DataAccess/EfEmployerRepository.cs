﻿using Microsoft.EntityFrameworkCore;
using ProyectCT.Core.Abstractions;
using ProyectCT.Core.Abstractions.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public class EfEmployerRepository : IEmployerRepository
    {
        private readonly ProyectCTMainDbContext _dbContext;

        public EfEmployerRepository(ProyectCTMainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Employer> GetByIdAsync(Guid EmployerID)
        {
            return await _dbContext.Employers.FirstOrDefaultAsync(d => d.EmployerID == EmployerID);
        }
      
        public async Task InsertAsync(Employer Employer)
        {
            //PersonalInformation.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());

            await _dbContext.Employers.AddAsync(Employer);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Employer>> ListAllAsync()
        {
            return await _dbContext.Employers.ToListAsync();
        }
       
        public async Task UpdateAsync(Employer Employer)
        {
            var EmployerOriginal = await _dbContext.Employers.FindAsync(Employer.EmployerID);
            //PersonalInformation.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());

            _dbContext.Employers.Update(EmployerOriginal);
            await _dbContext.SaveChangesAsync();
        }
    }
}
