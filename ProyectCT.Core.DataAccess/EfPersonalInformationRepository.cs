﻿using Microsoft.EntityFrameworkCore;
using ProyectCT.Auth.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public class EfPersonalInformationRepository : IPersonalInformationRepository
    {
        private readonly ProyectCTMainDbContext _dbContext;

        public EfPersonalInformationRepository(ProyectCTMainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PersonalInformation> GetByIdAsync(string UserID)
        {
            return await _dbContext.PersonalInformation.FirstOrDefaultAsync(d => d.UserID == UserID);
        }

        public async Task HardDeleteAsync(string UserID)
        {
            var PersonalInformation = await _dbContext.PersonalInformation.Where(u => u.UserID == UserID).SingleAsync();

            _dbContext.PersonalInformation.Remove(PersonalInformation);
            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(PersonalInformation PersonalInformation)
        {
            //PersonalInformation.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());

            await _dbContext.PersonalInformation.AddAsync(PersonalInformation);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<PersonalInformation>> ListAllAsync()
        {
            return await _dbContext.PersonalInformation.ToListAsync();
        }

        public async Task SoftDeleteAsync(string UserID)
        {
            var PersonalInformation = await _dbContext.PersonalInformation.Where(u => u.UserID == UserID).SingleAsync();

            //PersonalInformation.AuditInfo.MarkDeletion(_sessionService.GetCurrentSessionUserId());
            _dbContext.PersonalInformation.Update(PersonalInformation);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(PersonalInformation PersonalInformation)
        {
            var PersonalInformationOriginal = await _dbContext.PersonalInformation.FindAsync(PersonalInformation.UserID);
            //PersonalInformation.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());

            PersonalInformationOriginal.AreaTituloProfecion = PersonalInformation.AreaTituloProfecion;
            PersonalInformationOriginal.AvailabilityToTravel = PersonalInformation.AvailabilityToTravel;
            PersonalInformationOriginal.Birthdate = PersonalInformation.Birthdate;
            PersonalInformationOriginal.City = PersonalInformation.City;
            PersonalInformationOriginal.CivilStatus = PersonalInformation.CivilStatus;
            PersonalInformationOriginal.Country = PersonalInformation.Country;
            PersonalInformationOriginal.Departament = PersonalInformation.Departament;
            PersonalInformationOriginal.FirstName = PersonalInformation.FirstName;
            PersonalInformationOriginal.Gender = PersonalInformation.Gender;
            PersonalInformationOriginal.Identification = PersonalInformation.Identification;
            PersonalInformationOriginal.IdentificationType = PersonalInformation.IdentificationType;
            PersonalInformationOriginal.LastName = PersonalInformation.LastName;
            PersonalInformationOriginal.MinimumSalary = PersonalInformation.MinimumSalary;
            PersonalInformationOriginal.Nationality = PersonalInformation.Nationality;
            PersonalInformationOriginal.Phone = PersonalInformation.Phone;
            PersonalInformationOriginal.Prefix = PersonalInformation.Prefix;
            PersonalInformationOriginal.ResidenceChangeAvailability = PersonalInformation.ResidenceChangeAvailability;
            PersonalInformationOriginal.Skype = PersonalInformation.Skype;
            PersonalInformationOriginal.TitleProfecion = PersonalInformation.TitleProfecion;
                      

            _dbContext.PersonalInformation.Update(PersonalInformationOriginal);
            await _dbContext.SaveChangesAsync();
        }
    }
}
