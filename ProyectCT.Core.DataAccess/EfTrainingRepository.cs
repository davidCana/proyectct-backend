﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProyectCT.Auth.Logic;
using ProyectCT.Core.Abstractions;

namespace ProyectCT.Core.DataAccess
{
    public class EfTrainingRepository : ITrainingRepository
    {
        private readonly ProyectCTMainDbContext _dbContext;
        private readonly IAuthenticatedSessionService _sessionService;

        public EfTrainingRepository(ProyectCTMainDbContext dbContext, IAuthenticatedSessionService sessionService)
        {
            _dbContext = dbContext;
            _sessionService = sessionService;
        }

        public async Task<Training> GetByIdAsync(Guid trainingID)
        {
            return await _dbContext.Training.FindAsync(trainingID);
        }

        public async Task HardDeleteAsync(Guid trainingID)
        {
            var training = await _dbContext.Training.Where(u => u.TrainingID == trainingID).SingleAsync();

            _dbContext.Training.Remove(training);
            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(Training training)
        {
            training.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());

            await _dbContext.Training.AddAsync(training);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Training>> ListAllAsync()
        {
            return await _dbContext.Training.ToListAsync();
        }

        public async Task SoftDeleteAsync(Guid TrainingID)
        {
            var training = await _dbContext.Training.Where(u => u.TrainingID == TrainingID).SingleAsync();

            training.AuditInfo.MarkDeletion(_sessionService.GetCurrentSessionUserId());
            _dbContext.Training.Update(training);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Training training)
        {
            var trainingOriginal = await _dbContext.Training.FindAsync(training.TrainingID);
            training.AuditInfo.MarkCreationOrUpdate(_sessionService.GetCurrentSessionUserId());
            trainingOriginal = training;

            _dbContext.Training.Update(trainingOriginal);
            await _dbContext.SaveChangesAsync();
        }
    }
}
