﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyectCT.Core;
using ProyectCT.Core.Abstractions;
using ProyectCT.Core.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EducationLevelController : ControllerBase
    {
        private IEducationLevelManager _educationLevel;

        public EducationLevelController(IEducationLevelManager educationLevel)
        {
            _educationLevel = educationLevel;
        }

        // GET: api/EducationLevel
        [HttpGet]
        public  ActionResult<IEnumerable<EducationLevel>> Get()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;

            try
            {
                var personalInformation = _educationLevel.ListAllEducationLevelAsync(userId);
                
                return personalInformation.Result.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // GET: api/EducationLevel/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/EducationLevel
        [HttpPost]
        public async Task<Object> Post([FromBody]EducationLevel educationLevel)
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;

            try
            {
                await _educationLevel.AddAsync(educationLevel);
                return Ok("Sussces");
            }
            catch (Exception ex)
            {
                return new
                {
                    ex
                };
            }
        }

        // PUT: api/EducationLevel/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] PersonalInformation personalInformation)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
