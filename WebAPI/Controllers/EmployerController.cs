﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProyectCT.Core.Abstractions;
using ProyectCT.Core.Abstractions.Logic;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployerController : ControllerBase
    {
        private IEmployerManager _Employer;
        private UserManager<ApplicationUser> _userManager;
        private readonly ApplicationSettings _appSettings;

        public EmployerController(UserManager<ApplicationUser> userManager, IEmployerManager employer, IOptions<ApplicationSettings> appSettings)
        {
            _userManager = userManager;
            _Employer = employer;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        [Route("RegisterEmployer")]
        //POST : /api/ApplicationUser/Register
        public async Task<Object> PostApplicationEmployer(ApplicationEmployerrModel model)
        {

            var applicationEmployer = new Employer()
            {
                email = model.email,
                firstName = model.firstName,
                lastName = model.lastName,
                password = model.password,
                rifCompany = model.rifCompany,
                businessName = model.businessName,
                NameEmployer = model.NameEmployer,
                phone = model.phone
            };


            var applicationUser = new ApplicationUser()
            {
                UserName = model.email,
                Email = model.email,
                FullName = model.firstName + ' ' + model.lastName,
            };

            try
            {
                var IdUser = await RegisterUserEmployer(applicationUser, model.password);

                applicationEmployer.EmployerID = IdUser;

                await _Employer.InsertAsync(applicationEmployer);
                return new
                {
                    IdUser
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        private async Task<Guid> RegisterUserEmployer(ApplicationUser model, string password)
        {

            try
            {
                var result = await _userManager.CreateAsync(model, password);

                var data = await _userManager.FindByEmailAsync(model.Email);

                return Guid.Parse(data.Id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        [HttpPost]
        [Route("Login")]
        //POST : /api/ApplicationUser/Login
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.UserName);

            var employer = await _Employer.GetByIdAsync(Guid.Parse(user.Id));

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                if (employer != null)
                {
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                  {
                        new Claim("UserID",user.Id.ToString())
                  }),
                        Expires = DateTime.UtcNow.AddDays(1),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                    var token = tokenHandler.WriteToken(securityToken);
                    return Ok(new { token });
                }
                else
                {
                    return BadRequest(new { message = "El usuario no existe como Empresa" });
                }


            }
            else
                return BadRequest(new { message = "Username or password is incorrect." });
        }

        [HttpGet]
        //GET : /api/UserProfile
        [Authorize]
        public async Task<Object> GetEmployerProfile()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;

            var employer = await _Employer.GetByIdAsync(Guid.Parse(userId));

            return new
            {
                employer.businessName,
                employer.email,
                employer.EmployerID,
                employer.firstName,
                employer.lastName,
                employer.NameEmployer,
                employer.phone,
                employer.rifCompany
            };
        }

    }
}