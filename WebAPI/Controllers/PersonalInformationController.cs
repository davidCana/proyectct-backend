﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyectCT.Core;
using ProyectCT.Core.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonalInformationController : ControllerBase
    {
        private IPersonalInformationManager _personalInformation;

        public PersonalInformationController(IPersonalInformationManager personalInformation)
        {
            _personalInformation = personalInformation;
        }

        [HttpPost]
        [Route("AddUserProfile")]
        [Authorize]
        //GET : /api/UserProfile
        public async Task<Object> AddUserProfile([FromBody]PersonalInformation personalInformation)
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;

            try
            {
                await _personalInformation.AddAsync(personalInformation);
                return Ok("Sussces");
            }
            catch (Exception ex)
            {
                return new
                {
                    ex
                };
            }
        }

        [HttpGet]
        [Route("GetUserProfile")]
        [Authorize]
        //GET : /api/UserProfile
        public async Task<Object> GetUserProfile()
        {
            string userId = User.Claims.First(c => c.Type == "UserID").Value;

            try
            {
                var personalInformation = await _personalInformation.GetByIdAsync(userId);
                return Ok(new
                {
                    personalInformation.FirstName,
                    personalInformation.LastName,
                    personalInformation.IdentificationType,
                    personalInformation.Identification,
                    personalInformation.CivilStatus,
                    personalInformation.Birthdate,
                    personalInformation.Phone,
                    personalInformation.Prefix,
                    personalInformation.Skype,
                    personalInformation.Country,
                    personalInformation.City,
                    personalInformation.Departament,
                    personalInformation.Nationality,
                    personalInformation.Gender,
                    personalInformation.TitleProfecion,
                    personalInformation.AreaTituloProfecion,
                    personalInformation.MinimumSalary,
                    personalInformation.ResidenceChangeAvailability,
                    personalInformation.AvailabilityToTravel,
                });
            }
            catch (Exception ex)
            {
                return new
                {
                    ex
                };
            }



        }
    }
}