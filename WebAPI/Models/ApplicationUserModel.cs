﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class ApplicationUserModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string NewPassword { get; set; }


    }

    public class ApplicationEmployerrModel
    {
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string rifCompany { get; set; }
        public string businessName { get; set; }
        public string NameEmployer { get; set; }
        public string phone { get; set; }
    }

}
