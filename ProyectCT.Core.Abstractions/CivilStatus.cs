﻿using ProyectCT.Core.Abstractions.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core
{
    public enum CivilStatus
    {
        [Display(Description = "Single", ResourceType = typeof(Captions))]
        Single = 0,

        [Display(Description = "Married", ResourceType = typeof(Captions))]
        Married = 1,
    }
}
