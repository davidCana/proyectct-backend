﻿using ProyectCT.Common;
using ProyectCT.Common.Resources;
using ProyectCT.Core.Abstractions.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectCT.Core.Abstractions
{
    public class WorkExperience
    {
        [Key]
        [Display(
           Name = nameof(Captions.WorkExperience),
           ShortName = nameof(Captions.IdShortName),
           ResourceType = typeof(Captions))]
        public Guid WorkExperienceID { get; set; }

        /// <summary>
        /// Description Departament
        /// </summary>
        [Display(
           Name = nameof(Captions.UserID),
           ShortName = nameof(Captions.UserID),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string UserID { get; set; }

        /// <summary>
        /// Company
        /// </summary>
        [Display(
           Name = nameof(Captions.Departament),
           ShortName = nameof(Captions.Departament),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string Company { get; set; }

        /// <summary>
        /// Description Departament
        /// </summary>
        [Display(
           Name = nameof(Captions.Departament),
           ShortName = nameof(Captions.DepartamentID),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public int DepartamentID { get; set; }
        
        /// <summary>
        /// Description Departament
        /// </summary>
        [Display(
           Name = nameof(Captions.JobDescription),
           ShortName = nameof(Captions.JobDescription),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string JobDescription { get; set; }

        #region Evaluation Company

        /// <summary>
        /// Description Departament
        /// </summary>
        [Display(
           Name = nameof(Captions.WorkEnvironment),
           ShortName = nameof(Captions.WorkEnvironment),
           ResourceType = typeof(Captions))]
        public int WorkEnvironment { get; set; }

        /// <summary>
        /// Salary
        /// </summary>
        [Display(
           Name = nameof(Captions.Salary),
           ShortName = nameof(Captions.Salary),
           ResourceType = typeof(Captions))] 
        public int Salary { get; set; }

        /// <summary>
        /// Description Departament
        /// </summary>
        [Display(
           Name = nameof(Captions.CareerOpportunity),
           ShortName = nameof(Captions.CareerOpportunity),
           ResourceType = typeof(Captions))]
        public bool CareerOpportunity { get; set; }

        /// <summary>
        /// Description Departament
        /// </summary>
        [Display(
           Name = nameof(Captions.IsRecommend),
           ShortName = nameof(Captions.IsRecommend),
           ResourceType = typeof(Captions))]
        public bool IsRecommend { get; set; }
        #endregion

        /// <summary>
        /// Gets access to audit trail details for this entity.
        /// </summary>
        public AuditInfo AuditInfo { get; set; } = new AuditInfo();
    }
}
