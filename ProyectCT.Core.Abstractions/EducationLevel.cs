﻿using ProyectCT.Common;
using ProyectCT.Common.Resources;
using ProyectCT.Core.Abstractions.Resources;
using System.ComponentModel.DataAnnotations;

namespace ProyectCT.Core.Abstractions
{
    public class EducationLevel
    {
        [Key]
        public int EducationLevelID { get; set; }
        public string UserID { get; set; }
        public string Institute { get; set; }
        public string Titulation { get; set; }
        public string Description { get; set; }
        public string AcademyDiscipline { get; set; }
    }
}
