﻿using ProyectCT.Common;
using ProyectCT.Common.Resources;
using ProyectCT.Core.Abstractions.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProyectCT.Core
{
    public class PersonalInformation
    {
        [Key]
        public string UserID { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// IdentificationType select to list Identification
        /// </summary>
        public int IdentificationType { get; set; }

        /// <summary>
        /// IdentificationType select to list Identification
        /// </summary>
        public int Identification { get; set; }

        /// <summary>
        /// Select status Civil of enumerable CivilStatus
        /// </summary>
        public CivilStatus CivilStatus { get; set; }

        /// <summary>
        /// Birthdate select for combo
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// Select combo Skype
        /// </summary>
        public string Skype { get; set; }

        /// <summary>
        /// Select combo Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Select combo city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Departament
        /// </summary>
        public int Departament { get; set; }

        /// <summary>
        /// Nationality the of Candidat
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// Nationality the of Candidat
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// Nationality the of Candidat
        /// </summary>
        public string TitleProfecion { get; set; }

        /// <summary>
        /// Nationality the of Candidat
        /// </summary>
        public string AreaTituloProfecion { get; set; }

        /// <summary>
        /// Nationality the of Candidat
        /// </summary>
        public decimal MinimumSalary { get; set; }

        /// <summary>
        /// Nationality the of Candidat
        /// </summary>
        public int ResidenceChangeAvailability { get; set; }

       
        /// <summary>
        /// Nationality the of Candidat
        /// </summary>
        public int AvailabilityToTravel { get; set; }

        /// <summary>
        /// Gets access to audit trail details for this entity.
        /// </summary>
        //public AuditInfo AuditInfo { get; set; } = new AuditInfo();


        public List<MemberComparison> CompareMembers(PersonalInformation other)
        {
            return MemberComparer<PersonalInformation>.Compare(other, this)
             .Property(p => p.UserID)
             .Property(p => p.IdentificationType)
             .Property(p => p.CivilStatus)
             .Property(p => p.Birthdate)
             .Property(p => p.Phone)
             .Property(p => p.Country)
             .Property(p => p.City)
             .Property(p => p.Departament)
             .Property(p => p.Nationality)
             .GetDifferences();
        }

        public bool Equals(PersonalInformation other)
        {
            return MemberComparer<PersonalInformation>.CheckValueEquality(other, this)
             .Property(p => p.UserID)
             .Property(p => p.IdentificationType)
             .Property(p => p.CivilStatus)
             .Property(p => p.Birthdate)
             .Property(p => p.Phone)
             .Property(p => p.Country)
             .Property(p => p.City)
             .Property(p => p.Departament)
             .Property(p => p.Nationality)
             .AreEqual();
        }

        public override int GetHashCode()
        {
            return new HashCodeCalculator<PersonalInformation>(this)
             .Property(p => p.UserID)
             .Property(p => p.IdentificationType)
             .Property(p => p.CivilStatus)
             .Property(p => p.Birthdate)
             .Property(p => p.Phone)
             .Property(p => p.Country)
             .Property(p => p.City)
             .Property(p => p.Departament)
             .Property(p => p.Nationality)
             .GetFinalHash();
        }

    }
}
