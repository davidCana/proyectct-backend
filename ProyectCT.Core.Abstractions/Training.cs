﻿using ProyectCT.Common;
using ProyectCT.Common.Resources;
using ProyectCT.Core.Abstractions.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.Abstractions
{
    public class Training
    {
        /// <summary>
        /// Identifier of the Training (formacion).
        /// </summary>
        [Key]
        [Display(
           Name = nameof(Captions.Training),
           ShortName = nameof(Captions.IdShortName),
           ResourceType = typeof(Captions))]
        public Guid TrainingID { get; set; }

        /// <summary>
        /// Identifier of the User.
        /// </summary>
        [Display(
           Name = nameof(Captions.UserID),
           ShortName = nameof(Captions.IdShortName),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string UserID { get; set; }

        /// <summary>
        /// School (Formacion academica).
        /// </summary>
        [Display(
           Name = nameof(Captions.School),
           ShortName = nameof(Captions.School),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string School { get; set; }

        /// <summary>
        /// Education Level.
        /// </summary>
        [Display(
           Name = nameof(Captions.EducationLevel),
           ShortName = nameof(Captions.EducationLevelID),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public int EducationLevelID { get; set; }

        /// <summary>
        /// Education Level.
        /// </summary>
        [Display(
           Name = nameof(Captions.PeriodMonth),
           ShortName = nameof(Captions.PeriodMonth),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public int PeriodMonth { get; set; }

        /// <summary>
        /// Education Level.
        /// </summary>
        [Display(
           Name = nameof(Captions.PeriodYear),
           ShortName = nameof(Captions.PeriodYear),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public int PeriodYear { get; set; }

        /// <summary>
        /// Academic Status
        /// </summary>
        [Display(
           Name = nameof(Captions.State),
           ShortName = nameof(Captions.State),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public AcademicStatus State { get; set; }

        /// <summary>
        /// Gets access to audit trail details for this entity.
        /// </summary>
        public AuditInfo AuditInfo { get; set; } = new AuditInfo();


        public List<MemberComparison> CompareMembers(Training other)
        {
            return MemberComparer<Training>.Compare(other, this)
             .Property(p => p.TrainingID)
             .Property(p => p.UserID)
             .Property(p => p.School)
             .Property(p => p.EducationLevelID)
             .Property(p => p.PeriodMonth)
             .Property(p => p.PeriodYear)
             .Property(p => p.State)
             .GetDifferences();
        }

        public bool Equals(Training other)
        {
            return MemberComparer<Training>.CheckValueEquality(other, this)
             .Property(p => p.TrainingID)
             .Property(p => p.UserID)
             .Property(p => p.School)
             .Property(p => p.EducationLevelID)
             .Property(p => p.PeriodMonth)
             .Property(p => p.PeriodYear)
             .Property(p => p.State)
             .AreEqual();
        }

        public override int GetHashCode()
        {
            return new HashCodeCalculator<Training>(this)
             .Property(p => p.TrainingID)
             .Property(p => p.UserID)
             .Property(p => p.School)
             .Property(p => p.EducationLevelID)
             .Property(p => p.PeriodMonth)
             .Property(p => p.PeriodYear)
             .Property(p => p.State)
             .GetFinalHash();
        }
    }
}
