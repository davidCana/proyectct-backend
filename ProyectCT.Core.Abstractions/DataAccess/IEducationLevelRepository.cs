﻿using ProyectCT.Core.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public interface IEducationLevelRepository
    {
        Task InsertAsync(EducationLevel EducationLevel);
        Task UpdateAsync(EducationLevel EducationLevel);
      
        Task SoftDeleteAsync(int EducationLevelID);
        Task HardDeleteAsync(int EducationLevelID);
        Task<EducationLevel> GetByIdAsync(int EducationLevelID);

        Task<IEnumerable<EducationLevel>> ListAllAsync(string UserID);
    }
}
