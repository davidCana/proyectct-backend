﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public interface IDepartamentRepository
    {
        Task InsertAsync(Departament Departament);
        Task UpdateAsync(Departament Departament);

        Task SoftDeleteAsync(int DepartamentID);
        Task HardDeleteAsync(int DepartamentID);
        Task<Departament> GetByIdAsync(int DepartamentID);

        Task<IEnumerable<Departament>> ListAllAsync();
    }
}
