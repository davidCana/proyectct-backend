﻿using ProyectCT.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public interface IWorkExperienceRepository
    {
        Task InsertAsync(WorkExperience WorkExperience);
        Task UpdateAsync(WorkExperience WorkExperience);
  
        Task SoftDeleteAsync(Guid WorkExperienceID);
        Task HardDeleteAsync(Guid WorkExperienceID);
        Task<WorkExperience> GetByIdAsync(Guid WorkExperienceID);

        Task<IEnumerable<WorkExperience>> ListAllAsync();
    }
}
