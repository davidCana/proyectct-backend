﻿using ProyectCT.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public interface ITrainingRepository
    {
        Task InsertAsync(Training Training);
        Task UpdateAsync(Training Training);       

        Task SoftDeleteAsync(Guid TrainingID);
        Task HardDeleteAsync(Guid TrainingID);
        Task<Training> GetByIdAsync(Guid TrainingID);

        Task<IEnumerable<Training>> ListAllAsync();
    }
}
