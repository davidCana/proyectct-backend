﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectCT.Core.DataAccess
{
    public interface IPersonalInformationRepository
    {
        Task InsertAsync(PersonalInformation personalInformation);
        Task UpdateAsync(PersonalInformation personalInformation);
        
        Task SoftDeleteAsync(string UserID);
        Task HardDeleteAsync(string UserID);
        Task<PersonalInformation> GetByIdAsync(string UserID);

        Task<IEnumerable<PersonalInformation>> ListAllAsync();
    }
}
