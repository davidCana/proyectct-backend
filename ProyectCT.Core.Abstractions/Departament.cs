﻿using ProyectCT.Common.Resources;
using ProyectCT.Core.Abstractions.Resources;
using System.ComponentModel.DataAnnotations;

namespace ProyectCT.Core
{
    public class Departament
    {
        [Key]
        [Display(
           Name = nameof(Captions.Departament),
           ShortName = nameof(Captions.Departament),
           ResourceType = typeof(Captions))]
        public int DepartamentID { get; set; }

        /// <summary>
        /// Description Departament
        /// </summary>
        [Display(
           Name = nameof(Captions.Departament),
           ShortName = nameof(Captions.Departament),
           ResourceType = typeof(Captions))]
        [Required(AllowEmptyStrings = true,
           ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
           ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string Description { get; set; }
    }
}
