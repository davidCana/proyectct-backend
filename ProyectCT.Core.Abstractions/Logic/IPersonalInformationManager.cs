﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    public interface IPersonalInformationManager
    {
        Task AddAsync(PersonalInformation personalInformation);
        Task UpdateAsync(PersonalInformation personalInformation);
        Task DeleteAsync(string UserID);

        Task<PersonalInformation> GetByIdAsync(string UserID);
        Task<IEnumerable<PersonalInformation>> ListAllAsync();
    }
}
