﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.Abstractions.Logic
{
    public interface IEmployerManager
    {
        Task InsertAsync(Employer Employer);
        Task UpdateAsync(Employer Employer);

        Task<Employer> GetByIdAsync(Guid EmployerID);

        Task<IEnumerable<Employer>> ListAllAsync();
    }
}
