﻿using ProyectCT.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    public interface IDepartamentManager
    {
        Task AddAsync(Departament departament);
        Task UpdateAsync(Departament departament);
        Task DeleteAsync(int departamentID);

        Task<Departament> GetDepartamentAsync(int departamentID);
        Task<IEnumerable<Departament>> ListAllDepartamentAsync();
    }
}
