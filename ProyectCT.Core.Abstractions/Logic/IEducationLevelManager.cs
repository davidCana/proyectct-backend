﻿using ProyectCT.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    public interface IEducationLevelManager
    {
        Task AddAsync(EducationLevel educationLevel);
        Task UpdateAsync(EducationLevel educationLevel);
        Task DeleteAsync(int educationLevelID);

        Task<EducationLevel> GetEducationLevelAsync(int educationLevelID);
        Task<IEnumerable<EducationLevel>> ListAllEducationLevelAsync(string UserID);
    }
}
