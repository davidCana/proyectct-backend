﻿using ProyectCT.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    public interface ITrainingManager
    {
        Task AddAsync(Training training);
        Task UpdateAsync(Training training);
        Task DeleteAsync(Guid trainingID);

        Task<Training> GetByIdAsync(Guid trainingID);
        Task<IEnumerable<Training>> ListAllAsync();
    }
}
