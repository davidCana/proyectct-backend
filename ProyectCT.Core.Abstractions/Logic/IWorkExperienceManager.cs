﻿using ProyectCT.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    public interface IWorkExperienceManager
    {
        Task AddAsync(WorkExperience workExperience);
        Task UpdateAsync(WorkExperience workExperience);        
        Task DeleteAsync(Guid workExperienceID);
        
        Task<WorkExperience> GetByIdAsync(Guid workExperienceID);
        Task<IEnumerable<WorkExperience>> ListAllAsync();

    }
}
