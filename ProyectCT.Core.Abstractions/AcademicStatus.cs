﻿using ProyectCT.Core.Abstractions.Resources;
using System.ComponentModel.DataAnnotations;

namespace ProyectCT.Core.Abstractions
{
    public enum AcademicStatus
    {
        [Display(Description = "Studying", ResourceType = typeof(Captions))]
        Studying = 0,

        [Display(Description = "Completed", ResourceType = typeof(Captions))]
        Completed = 1,
    }
}
