﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProyectCT.Core.Abstractions
{
    public class Employer
    {
        [Key]
        public Guid EmployerID { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string rifCompany { get; set; }
        public string businessName { get; set; }
        public string NameEmployer { get; set; }
        public string phone { get; set; }
    }
}
