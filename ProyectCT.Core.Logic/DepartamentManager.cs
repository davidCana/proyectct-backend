﻿using ProyectCT.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectCT.Common;
using ProyectCT.Common.Extensions;

namespace ProyectCT.Core.Logic
{
    public class DepartamentManager : IDepartamentManager
    {
        private readonly IDepartamentRepository _IDepartamentRepository;

        public DepartamentManager(IDepartamentRepository departamentRepository)
        {
            _IDepartamentRepository = departamentRepository;
        }

        public async Task AddAsync(Departament departament)
        {
            var validationErrors = departament.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors);

            await _IDepartamentRepository.InsertAsync(departament);

        }

        public async Task DeleteAsync(int departamentID)
        {
            //using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            await _IDepartamentRepository.SoftDeleteAsync(departamentID);
            //}
        }

        public async Task<Departament> GetDepartamentAsync(int departamentID)
        {
            return await _IDepartamentRepository.GetByIdAsync(departamentID);
        }

        public async Task<IEnumerable<Departament>> ListAllDepartamentAsync()
        {
            return await _IDepartamentRepository.ListAllAsync();
        }

        public async Task UpdateAsync(Departament departament)
        {
            var departamentOriginal = _IDepartamentRepository.GetByIdAsync(departament.DepartamentID);

            if (departamentOriginal == null)
                throw new UserFriendlyException(string.Format( "No se encuentra el Departamento", departament.DepartamentID));
            
            await _IDepartamentRepository.UpdateAsync(departament);
        }
    }
}
