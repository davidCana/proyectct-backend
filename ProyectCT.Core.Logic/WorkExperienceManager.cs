﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectCT.Common;
using ProyectCT.Common.Extensions;
using ProyectCT.Core.Abstractions;
using ProyectCT.Core.DataAccess;

namespace ProyectCT.Core.Logic
{
    public class WorkExperienceManager : IWorkExperienceManager
    {
        private readonly IWorkExperienceRepository _IWorkExperienceRepository;

        public WorkExperienceManager(IWorkExperienceRepository workExperience)
        {
            _IWorkExperienceRepository = workExperience;
        }

        public async Task AddAsync(WorkExperience workExperience)
        {
            if (workExperience.WorkExperienceID == Guid.Empty) workExperience.WorkExperienceID = Guid.NewGuid().AsSequential();

            var validationErrors = workExperience.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors);

            await _IWorkExperienceRepository.InsertAsync(workExperience);
        }

        public async Task DeleteAsync(Guid workExperience)
        {
            //using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            await _IWorkExperienceRepository.SoftDeleteAsync(workExperience);
            //}
        }

        public async Task<WorkExperience> GetByIdAsync(Guid workExperience)
        {
            return await _IWorkExperienceRepository.GetByIdAsync(workExperience);
        }

        public async Task<IEnumerable<WorkExperience>> ListAllAsync()
        {
            return await _IWorkExperienceRepository.ListAllAsync();
        }

        public async Task UpdateAsync(WorkExperience workExperience)
        {


            var validationErrors = workExperience.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors.ToList());

            var original = await _IWorkExperienceRepository.GetByIdAsync(workExperience.WorkExperienceID);
            if (original == null)
            {
                throw new UserFriendlyException(string.Format("No se encuentra la experiencia", workExperience.WorkExperienceID));
            }

            if (original.AuditInfo.ConcurrencyStamp != workExperience.AuditInfo.ConcurrencyStamp
                || original.AuditInfo.Version != workExperience.AuditInfo.Version)
            {
                throw new UserFriendlyException(string.Format("No se encuentra la experiencia", workExperience.WorkExperienceID));
            }

            //If none of the properties of the user has changed, then do nothing
            if (original.Equals(workExperience))
            {
                return;
            }

            if (original == null)
                throw new UserFriendlyException(string.Format("No se encuentra la experiencia", workExperience.WorkExperienceID));

            await _IWorkExperienceRepository.UpdateAsync(workExperience);
        }

    }
}
