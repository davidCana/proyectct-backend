﻿using ProyectCT.Common;
using ProyectCT.Common.Extensions;
using ProyectCT.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    public class PersonalInformationManager : IPersonalInformationManager
    {
        private readonly IPersonalInformationRepository _IPersonalInformationRepository;

        public PersonalInformationManager(IPersonalInformationRepository personalInformation)
        {
            _IPersonalInformationRepository = personalInformation;
        }

        public async Task AddAsync(PersonalInformation perosnalInformation)
        {
            var validationErrors = perosnalInformation.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors);

            var personalInf = await _IPersonalInformationRepository.GetByIdAsync(perosnalInformation.UserID);

            if (personalInf == null)
                await _IPersonalInformationRepository.InsertAsync(perosnalInformation);
            else
                await _IPersonalInformationRepository.UpdateAsync(perosnalInformation);
        }

        public async Task DeleteAsync(string UserID)
        {
            //using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            await _IPersonalInformationRepository.SoftDeleteAsync(UserID);
            //}
        }

        public async Task<PersonalInformation> GetByIdAsync(string UserID)
        {
            return await _IPersonalInformationRepository.GetByIdAsync(UserID);
        }

        public async Task<IEnumerable<PersonalInformation>> ListAllAsync()
        {
            return await _IPersonalInformationRepository.ListAllAsync();
        }

        public async Task UpdateAsync(PersonalInformation personalInformation)
        {
            var personalInformationOriginal = _IPersonalInformationRepository.GetByIdAsync(personalInformation.UserID);

            if (personalInformationOriginal == null)
                throw new UserFriendlyException(string.Format("No se encuentra el Candidato {UserID}", personalInformation.UserID));

            await _IPersonalInformationRepository.UpdateAsync(personalInformation);
        }
    }
}
