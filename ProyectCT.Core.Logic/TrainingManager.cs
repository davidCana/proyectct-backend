﻿using ProyectCT.Common;
using ProyectCT.Common.Extensions;
using ProyectCT.Core.Abstractions;
using ProyectCT.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    public class TrainingManager : ITrainingManager
    {
        private readonly ITrainingRepository _ITrainingRepository;

        public TrainingManager(ITrainingRepository training)
        {
            _ITrainingRepository = training;
        }

        public async Task AddAsync(Training training)
        {
            if (training.TrainingID == Guid.Empty) training.TrainingID = Guid.NewGuid().AsSequential();

            var validationErrors = training.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors);

            await _ITrainingRepository.InsertAsync(training);
        }

        public async Task DeleteAsync(Guid trainingID)
        {
            //using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            await _ITrainingRepository.SoftDeleteAsync(trainingID);
            //}
        }

        public async Task<Training> GetByIdAsync(Guid training)
        {
            return await _ITrainingRepository.GetByIdAsync(training);
        }

        public async Task<IEnumerable<Training>> ListAllAsync()
        {
            return await _ITrainingRepository.ListAllAsync();
        }

        public async Task UpdateAsync(Training training)
        {
           

            var validationErrors = training.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors.ToList());

            var original = await _ITrainingRepository.GetByIdAsync(training.TrainingID);
            if (original == null)
            {
                throw new UserFriendlyException(string.Format("No se encuentra el TraningID", training.TrainingID));
            }

            if (original.AuditInfo.ConcurrencyStamp != training.AuditInfo.ConcurrencyStamp
                || original.AuditInfo.Version != training.AuditInfo.Version)
            {
                throw new UserFriendlyException(string.Format("No se encuentra el TraningID", training.TrainingID));
            }

            //If none of the properties of the user has changed, then do nothing
            if (original.Equals(training))
            {
                return;
            }

            if (original == null)
                throw new UserFriendlyException(string.Format("No se encuentra el TraningID", training.TrainingID));

            await _ITrainingRepository.UpdateAsync(training);
        }
    }
}
