﻿using Microsoft.Extensions.DependencyInjection;
using ProyectCT.Core.Abstractions.Logic;

namespace ProyectCT.Core.Logic
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection RegisterProyectCTLogicServices(this IServiceCollection services)
        {
            services.AddTransient<IPersonalInformationManager, PersonalInformationManager>();
            services.AddTransient<IEmployerManager, EmployerManager>();
            services.AddTransient<IEducationLevelManager, EducationLevelManager>();
            return services;
        }
    }
}
