﻿using ProyectCT.Common;
using ProyectCT.Common.Extensions;
using ProyectCT.Core.Abstractions;
using ProyectCT.Core.Abstractions.DataAccess;
using ProyectCT.Core.Abstractions.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Core.Logic
{
    class EmployerManager : IEmployerManager
    {
        private readonly IEmployerRepository _IEmployerRepository;

        public EmployerManager(IEmployerRepository employerRepository)
        {
            _IEmployerRepository = employerRepository;
        }

        public async Task InsertAsync(Employer departament)
        {
            var validationErrors = departament.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors);

            await _IEmployerRepository.InsertAsync(departament);

        }

        public async Task<Employer> GetByIdAsync(Guid EmployerID)
        {
            return await _IEmployerRepository.GetByIdAsync(EmployerID);
        }

        public async Task<IEnumerable<Employer>> ListAllAsync()
        {
            return await _IEmployerRepository.ListAllAsync();
        }

        public async Task UpdateAsync(Employer Employer)
        {
            var departamentOriginal = _IEmployerRepository.GetByIdAsync(Employer.EmployerID);

            if (departamentOriginal == null)
                throw new UserFriendlyException(string.Format("No se encuentra la empresa", Employer.EmployerID));

            await _IEmployerRepository.UpdateAsync(Employer);
        }
    }

}
