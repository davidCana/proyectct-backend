﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectCT.Common;
using ProyectCT.Common.Extensions;
using ProyectCT.Core.Abstractions;
using ProyectCT.Core.DataAccess;

namespace ProyectCT.Core.Logic
{
    public class EducationLevelManager : IEducationLevelManager
    {
        private readonly IEducationLevelRepository _IEducationLevelRepository;

        public EducationLevelManager(IEducationLevelRepository educationLevel)
        {
            _IEducationLevelRepository = educationLevel;
        }

        public async Task AddAsync(EducationLevel educationLevel)
        {
            var validationErrors = educationLevel.ValidateDataAnnotations();
            if (validationErrors.Any()) throw new ExtendedValidationException(validationErrors);

            await _IEducationLevelRepository.InsertAsync(educationLevel);
        }

        public async Task DeleteAsync(int educationlevelID)
        {
            //using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            await _IEducationLevelRepository.SoftDeleteAsync(educationlevelID);
            //}
        }

        public async Task<EducationLevel> GetEducationLevelAsync(int educationLevelID)
        {
            return await _IEducationLevelRepository.GetByIdAsync(educationLevelID);
        }

        public async Task<IEnumerable<EducationLevel>> ListAllEducationLevelAsync(string UserID)
        {
            return await _IEducationLevelRepository.ListAllAsync(UserID);
        }

        public async Task UpdateAsync(EducationLevel educationLevel)
        {
            var departamentOriginal = _IEducationLevelRepository.GetByIdAsync(educationLevel.EducationLevelID);

            if (departamentOriginal == null)
                throw new UserFriendlyException(string.Format("No se encuentra nivel de Eduacion", educationLevel.EducationLevelID));

            await _IEducationLevelRepository.UpdateAsync(educationLevel);
        }
    }
}
