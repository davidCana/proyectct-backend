﻿using ProyectCT.Common.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProyectCT.Common.FileManagement
{
    /// <summary>
    /// This model represents a file that is uploaded/stored in any of the storage
    /// providers of the system.
    /// </summary>
    public class File
    {
        /// <summary>
        /// Unique identifier of the file.
        /// </summary>
        [Key]
        [Display(
            Name = nameof(CommonCaptions.FileId),
            ShortName = nameof(CommonCaptions.IdShortName),
            ResourceType = typeof(CommonCaptions))]
        [Required(
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public Guid FileId { get; set; }

        /// <summary>
        /// Original name of the file.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.Name),
            ResourceType = typeof(CommonCaptions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.FileNameMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string Name { get; set; }

        /// <summary>
        /// Url that can be used to access the content of the file.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.Url),
            ResourceType = typeof(CommonCaptions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.UrlMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string Url { get; set; }

        /// <summary>
        /// The size in bytes of the file.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.Size),
            ResourceType = typeof(CommonCaptions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public uint Size { get; set; }

        /// <summary>
        /// The MIME content type of the file (i.e. 'image/png').
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.ContentType),
            ResourceType = typeof(CommonCaptions))]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.ContentTypeMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string ContentType { get; set; }

        /// <summary>
        /// A symbol/drawing/image that can identify visually the file.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.Icon),
            ResourceType = typeof(CommonCaptions))]
        public Icon Icon { get; set; }

        /// <summary>
        /// Optional. Gets or sets a contextual reference of the file in regard to its usage in the system. 
        /// This property can be used, i.e. to store some ID or name of the object that refers or is 
        /// linked to this file.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.ContextRef),
            ResourceType = typeof(CommonCaptions))]
        [MaxLength(
            StandardLimits.TechnicalReference,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string ContextRef { get; set; }

        /// <summary>
        /// Relative path of the file in the context of the storage provider.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.Path),
            ResourceType = typeof(CommonCaptions))]
        [Required(AllowEmptyStrings = true,
            ErrorMessageResourceName = nameof(CommonErrorMessages.FieldIsRequired),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        [MaxLength(
            StandardLimits.UrlMaxLength,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string Path { get; set; }

        /// <summary>
        /// Optional. Gets or sets an additional reference or information of this file 
        /// relative to the storage provider.
        /// </summary>
        [Display(
            Name = nameof(CommonCaptions.ProviderRef),
            ResourceType = typeof(CommonCaptions))]
        [MaxLength(
            StandardLimits.TechnicalReference,
            ErrorMessageResourceName = nameof(CommonErrorMessages.TextFieldExceedsMaxLength),
            ErrorMessageResourceType = typeof(CommonErrorMessages))]
        public string ProviderRef { get; set; }

        /// <summary>
        /// Gets access to audit trail details for this entity.
        /// </summary>
        public AuditInfo AuditInfo { get; set; }
    }
}
