﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ProyectCT.Common
{
    /// <summary>
    /// Represents the comparison of values between two different objects
    /// for a specific property of their type.
    /// </summary>
    public class MemberComparison : IEquatable<MemberComparison>
    {
        #region Properties

        /// <summary>
        /// The property or field which was compared.
        /// </summary>
        public MemberInfo Member { get; set; }

        /// <summary>
        /// Gets the invariant name of the property or field. This invariant name is equivalent to the member name.
        /// </summary>
        public string InvariantName => Member?.Name;

        /// <summary>
        /// Gets the localized name for the property/field, according to the DisplayAttribute assigned to the member, 
        /// either from the implementation on the concrete class or from an implemented interface.
        /// </summary>
        public string Name => Member?.GetDisplayName();

        /// <summary>
        /// Gets the localized short name for the property/field, according to the DisplayAttribute assigned to the member, 
        /// either from the implementation on the concrete class or from an implemented interface.
        /// </summary>
        public string ShortName => Member?.GetDisplayShortName();

        /// <summary>
        /// Gets the value assigned to the property/field for the source object of the comparison.
        /// </summary>
        public object SourceValue { get; set; }

        /// <summary>
        /// Gets the value assigned to the property/field for the target object of the comparison.
        /// </summary>
        public object TargetValue { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a MemberComparison object, specifying the metadata of the member (property or field) that was compared,
        /// as well as its values for the source and target objects.
        /// </summary>
        /// <param name="member">Reflection metadata of the property or field being compared</param>
        /// <param name="sourceValue">Value of the property/field in the source object</param>
        /// <param name="targetValue">Value of the property/field in the target object</param>
        public MemberComparison(MemberInfo member, object sourceValue, object targetValue)
        {
            Member = member;
            SourceValue = sourceValue;
            TargetValue = targetValue;
        }

        public MemberComparison() { }

        #endregion

        #region Methods

        public override string ToString()
        {
            return $"{InvariantName}: {SourceValue} => {TargetValue}";
        }

        public override int GetHashCode()
        {
            return new HashCodeCalculator<MemberComparison>(this)
                .Property(p => p.Member)
                .Property(p => p.SourceValue)
                .Property(p => p.TargetValue)
                .GetFinalHash();
        }

        public bool Equals(MemberComparison other)
        {
            return MemberComparer<MemberComparison>.CheckValueEquality(other, this)
                .Property(p => p.Member)
                .Property(p => p.SourceValue)
                .Property(p => p.TargetValue)
                .AreEqual();
        }

        #endregion
    }

    /// <summary>
    /// Class which contains the logic to compare two objects of a same type based on a provided list of properties/fields,
    /// and determines the differences for each of those members between them.
    /// </summary>
    /// <typeparam name="T">Type of the objects to be compared</typeparam>
    public class MemberComparer<T>
    {
        #region Fields

        private readonly T _source;
        private readonly T _target;
        private readonly bool _onlyCompareEquality;
        private readonly List<MemberComparison> _differences;
        private bool _areEqual;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates an instance of the member comparer object, being provided both the source and target objects.
        /// </summary>
        /// <param name="source">Source object of the comparison</param>
        /// <param name="target">Target object of the comparison</param>
        /// <param name="onlyCompareEquality">If <c>true</c>, the comparisons will check only the equality
        /// of values without providing a list of memberwise differences.
        /// If <c>false</c> (default value), it will provide this list of differences,
        /// so it can be used for logging or history archiving purposes.</param>
        private MemberComparer(T source, T target, bool onlyCompareEquality = false)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (target == null) throw new ArgumentNullException(nameof(target));

            _source = source;
            _target = target;

            _areEqual = true;
            _onlyCompareEquality = onlyCompareEquality;

            if (!_onlyCompareEquality) _differences = new List<MemberComparison>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method registers a property to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TProperty">Type of the property being compared</typeparam>
        /// <param name="property">Lambda expression indicating the property being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public MemberComparer<T> Property<TProperty>(Expression<Func<T, TProperty>> property)
        {
            var member = property.Body as MemberExpression;
            var propertyInfo = member?.Member as PropertyInfo;
            if (propertyInfo == null) return this;

            var thisValue = propertyInfo.GetValue(_source);
            var otherValue = propertyInfo.GetValue(_target);

            if (thisValue == null && otherValue == null) return this;

            if (thisValue == null || otherValue == null)
            {
                _areEqual = false;
                if (!_onlyCompareEquality) _differences.Add(new MemberComparison(propertyInfo, thisValue, otherValue));
                return this;
            }

            TProperty otherValueCasted = (TProperty)otherValue;

            if ((thisValue is IEquatable<TProperty> thisValueEquatable && !thisValueEquatable.Equals(otherValueCasted))
                || (!(thisValue is IEquatable<TProperty>) && !thisValue.Equals(otherValue)))
            {
                _areEqual = false;
                if (!_onlyCompareEquality) _differences.Add(new MemberComparison(propertyInfo, thisValue, otherValue));
            }

            return this;
        }

        /// <summary>
        /// This method registers a field to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TField">Type of the field being compared</typeparam>
        /// <param name="field">Lambda expression indicating the field being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public MemberComparer<T> Field<TField>(Expression<Func<T, TField>> field)
        {
            var member = field.Body as MemberExpression;
            var fieldInfo = member?.Member as FieldInfo;
            if (fieldInfo == null) return this;

            var sourceValue = fieldInfo.GetValue(_source);
            var targetValue = fieldInfo.GetValue(_target);

            if (sourceValue == null && targetValue == null) return this;

            if (sourceValue == null || targetValue == null)
            {
                _areEqual = false;
                if (!_onlyCompareEquality) _differences.Add(new MemberComparison(fieldInfo, sourceValue, targetValue));
                return this;
            }

            TField targetValueCasted = (TField)targetValue;

            if ((sourceValue is IEquatable<TField> sourceValueEquatable && !sourceValueEquatable.Equals(targetValueCasted))
                || (!(sourceValue is IEquatable<TField>) && !sourceValue.Equals(targetValue)))
            {
                _areEqual = false;
                if (!_onlyCompareEquality) _differences.Add(new MemberComparison(fieldInfo, sourceValue, targetValue));
            }

            return this;
        }

        /// <summary>
        /// This method registers a property to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TProperty">Type of the property being compared</typeparam>
        /// <typeparam name="TItems">Type of the collected objects in the list property being compared</typeparam>
        /// <param name="property">Lambda expression indicating the property being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public MemberComparer<T> ListProperty<TProperty, TItems>(Expression<Func<T, TProperty>> property)
            where TProperty : IEnumerable<TItems>
        {
            var member = property.Body as MemberExpression;
            var propertyInfo = member?.Member as PropertyInfo;
            if (propertyInfo == null) return this;
            var thisValue = propertyInfo.GetValue(_source) as IEnumerable<TItems>;
            var otherValue = propertyInfo.GetValue(_target) as IEnumerable<TItems>;
            if (thisValue == null && otherValue == null) return this;
            if (thisValue == null || otherValue == null
                || thisValue.Count() != otherValue.Count() || thisValue.Except(otherValue).Any())
            {
                _areEqual = false;
                if (!_onlyCompareEquality) _differences.Add(new MemberComparison(propertyInfo, thisValue, otherValue));
                return this;
            }

            return this;
        }

        /// <summary>
        /// This method registers a property to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TProperty">Type of the property being compared</typeparam>
        /// <typeparam name="TKeys">Type of the collected keys in the dictionary property being compared</typeparam>
        /// <typeparam name="TItems">Type of the collected values in the dictionary property being compared</typeparam>
        /// <param name="property">Lambda expression indicating the property being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public MemberComparer<T> MapProperty<TKeys, TItems>(Expression<Func<T, IDictionary<TKeys, TItems>>> property)
        {
            var member = property.Body as MemberExpression;
            var propertyInfo = member?.Member as PropertyInfo;
            if (propertyInfo == null) return this;

            var thisMap = propertyInfo.GetValue(_source) as IDictionary<TKeys, TItems>;
            var otherMap = propertyInfo.GetValue(_target) as IDictionary<TKeys, TItems>;

            if (thisMap == null && otherMap == null) return this;

            if (thisMap == null || otherMap == null
                || thisMap.Count != otherMap.Count || thisMap.Except(otherMap).Any())
            {
                _areEqual = false;
                if (!_onlyCompareEquality) _differences.Add(new MemberComparison(propertyInfo, thisMap, otherMap));
                return this;
            }

            return this;
        }

        /// <summary>
        /// Returns the list of differences found between the source and target objects.
        /// </summary>
        /// <returns>IEnumerable of MemberComparison objects, containing the metadata of the 
        /// properties/fields that are different between the source and target objects, as well as
        /// the values of those objects for that property.</returns>
        public List<MemberComparison> GetDifferences()
        {
            return _differences;
        }

        /// <summary>
        /// Returns the list of differences found between the source and target objects.
        /// </summary>
        /// <returns>IEnumerable of MemberComparison objects, containing the metadata of the 
        /// properties/fields that are different between the source and target objects, as well as
        /// the values of those objects for that property.</returns>
        public bool AreEqual()
        {
            return _areEqual;
        }

        public static MemberComparer<T> Compare(T source, T target)
        {
            return new MemberComparer<T>(source, target);
        }

        public static MemberComparer<T> CheckValueEquality(T source, T target)
        {
            return new MemberComparer<T>(source, target, true);
        }

        public MemberComparer<T> ListCompare<TProperty, TItems>(Expression<Func<T, TProperty>> property)
            where TProperty : IEnumerable<TItems>
        {
            var member = property.Body as MemberExpression;
            var propertyInfo = member?.Member as PropertyInfo;
            if (propertyInfo == null) return this;

            var thisValue = propertyInfo.GetValue(_source);
            var otherValue = propertyInfo.GetValue(_target);

            if (thisValue == null && otherValue == null) return this;

            if (!_onlyCompareEquality) _differences.Add(new MemberComparison(propertyInfo, thisValue, otherValue));
            return this;
        }

        #endregion
    }
}
