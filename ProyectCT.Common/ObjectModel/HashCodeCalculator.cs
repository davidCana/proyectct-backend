﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace ProyectCT.Common
{
    /// <summary>
    /// Class which contains the logic to generate a hash value for an object, considering
    /// the value of its relevant properties.
    /// </summary>
    /// <typeparam name="T">Type of the object for which a hash value will be calculated</typeparam>
    public class HashCodeCalculator<T>
    {
        #region Fields

        private readonly T _object;
        private int _hashCode;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates an instance of the hash code calculator object, being provided the source object.
        /// </summary>
        public HashCodeCalculator(T @object)
        {
            if (@object == null) throw new ArgumentNullException(nameof(@object));

            _object = @object;
            _hashCode = 13;
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method registers a property to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TProperty">Type of the property being compared</typeparam>
        /// <param name="property">Lambda expression indicating the property being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public HashCodeCalculator<T> Property<TProperty>(Expression<Func<T, TProperty>> property)
        {
            var member = property.Body as MemberExpression;
            var propertyInfo = member?.Member as PropertyInfo;
            if (propertyInfo == null) return this;

            var value = propertyInfo.GetValue(_object);

            unchecked
            {
                _hashCode = (_hashCode * 397)
                    ^ (ReferenceEquals(null, value)
                        ? 0
                        : value.GetHashCode());
            }

            return this;
        }

        /// <summary>
        /// This method registers a field to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TField">Type of the field being compared</typeparam>
        /// <param name="field">Lambda expression indicating the field being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public HashCodeCalculator<T> Field<TField>(Expression<Func<T, TField>> field)
        {
            var member = field.Body as MemberExpression;
            var fieldInfo = member?.Member as FieldInfo;
            if (fieldInfo == null) return this;

            var value = fieldInfo.GetValue(_object);

            unchecked
            {
                _hashCode = (_hashCode * 397)
                    ^ (ReferenceEquals(null, value)
                        ? 0
                        : value.GetHashCode());
            }

            return this;
        }

        /// <summary>
        /// This method registers a property to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TProperty">Type of the property being compared</typeparam>
        /// <typeparam name="TItems">Type of the collected objects of the property</typeparam>
        /// <param name="property">Lambda expression indicating the property being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public HashCodeCalculator<T> ListProperty<TProperty, TItems>(Expression<Func<T, TProperty>> property)
            where TProperty : IEnumerable<TItems>
        {
            var member = property.Body as MemberExpression;
            var propertyInfo = member?.Member as PropertyInfo;
            if (propertyInfo == null) return this;

            var value = (propertyInfo.GetValue(_object) as IEnumerable<TItems>)?.ToList();

            var valueIsNull = ReferenceEquals(null, value);

            unchecked
            {
                _hashCode = (_hashCode * 397)
                    ^ (valueIsNull
                        ? 0
                        : value.Count.GetHashCode());

                if (valueIsNull) return this;

                foreach (var item in value)
                {
                    _hashCode = (_hashCode * 397)
                    ^ (ReferenceEquals(null, item)
                        ? 0
                        : item.GetHashCode());
                }
            }

            return this;
        }

        /// <summary>
        /// This method registers a property to be compared as to determine if their values are
        /// equal between the target and source object.
        /// </summary>
        /// <typeparam name="TProperty">Type of the property being compared</typeparam>
        /// <typeparam name="TKeys">Type of the keys in the dictionary of the property</typeparam>
        /// <typeparam name="TItems">Type of the values in the dictionary of the property</typeparam>
        /// <param name="property">Lambda expression indicating the property being compared</param>
        /// <returns>The instance of the ObjectMemberComparer object (to allow chaining)</returns>
        public HashCodeCalculator<T> MapProperty<TProperty, TKeys, TItems>(Expression<Func<T, TProperty>> property)
            where TProperty : IDictionary<TKeys, TItems>
        {
            var member = property.Body as MemberExpression;
            var propertyInfo = member?.Member as PropertyInfo;
            if (propertyInfo == null) return this;

            var value = propertyInfo.GetValue(_object) as IDictionary<TKeys, TItems>;

            var valueIsNull = ReferenceEquals(null, value);

            unchecked
            {
                _hashCode = (_hashCode * 397)
                    ^ (valueIsNull
                        ? 0
                        : value.Count.GetHashCode());

                if (valueIsNull) return this;

                foreach (var item in value)
                {
                    _hashCode = (_hashCode * 397)
                        ^ (ReferenceEquals(null, item.Key)
                            ? 0
                            : item.Key.GetHashCode())
                        ^ (ReferenceEquals(null, item.Value)
                            ? 0
                            : item.Value.GetHashCode());
                }
            }

            return this;
        }

        /// <summary>
        /// Returns a hash value that was calculated considering all the registered properties 
        /// of the object.
        /// </summary>
        public int GetFinalHash()
        {
            return _hashCode;
        }

        #endregion
    }
}
