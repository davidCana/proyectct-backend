﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Common
{
    /// <summary>
    /// Provides extension methods to validate objects using the 
    /// standard data annotations.
    /// </summary>
    public static class DataAnnotationsValidator
    {
        /// <summary>
        /// Validates the provided object using the data annotations 
        /// </summary>
        /// <returns></returns>
        public static List<ValidationResult> ValidateDataAnnotations(this object @object, IServiceProvider contextServiceProvider = null, Dictionary<object, object> contextItems = null, List<ValidationResult> results = null)
        {
            var context = new ValidationContext(@object, contextServiceProvider, contextItems);
            if (results == null) results = new List<ValidationResult>();

            Validator.TryValidateObject(@object, context, results, true);
            return results;
        }
    }
}
