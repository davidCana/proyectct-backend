﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectCT.Common
{
    /// <summary>
    /// Interface that enforces implementation of <see cref="IEquatable{T}"/> (for defining a
    /// value comparison with <c>bool Equals(T object)</c> method), as well as enforcing
    /// a method to do the memberwise comparison of values, returning a list of the members which
    /// have different values between the objects.
    /// </summary>
    /// <typeparam name="T">Type of the objects to compare</typeparam>
    public interface IMemberwiseComparable<T> : IEquatable<T>
    {
        /// <summary>
        /// Performs a memberwise value comparison of the current object with the one provided
        /// in the parameter <paramref name="other"/> and returns details about the differences found.
        /// </summary>
        /// <param name="other">The object with which the current instance will be compared</param>
        /// <returns>List of <see cref="MemberComparison"/> objects detailing the differences that have been found
        /// between the objects</returns>
        List<MemberComparison> CompareMembers(T other);
    }
}
