﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ProyectCT.Common
{
    /// <summary>
    /// Object that includes basic audit and trace information about an object 
    /// (sequence number of the version of the entity, the time and user that created,
    /// modified by the last time and soft deleted an object).
    /// </summary>
    public class AuditInfo
    {
        /// <summary>
        /// Gets or sets the version number of the entity. This number should be increased by one
        /// only when an update operation of the entity is persisted and there was a change on at
        /// least one of the entity's properties.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Gets or sets a value that will help identify concurrency conflicts on simultaneous 
        /// write operations.
        /// </summary>
        public uint ConcurrencyStamp { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the entity was created.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the ID of the user that created the entity.
        /// </summary>
        public Guid CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the entity was updated by the very last time.
        /// </summary>
        public DateTime LastUpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets the ID of the user that updated the entity by the very last time.
        /// </summary>
        public Guid LastUpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating if the entity is soft-deleted (<c>true</c>) or not.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Optional. Gets or sets the date and time when the entity was soft-deleted.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime? DeletedAt { get; set; }

        /// <summary>
        /// Optional. Gets or sets the ID of the user that soft-deleted the entity.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid? DeletedBy { get; set; }

        /// <summary>
        /// Provided the user name of the current user, it sets the Created and LastUpdated values
        /// accordingly, using the UTC time.
        /// </summary>
        /// <param name="userId">ID of the user that is creating or updating the entity,
        /// to include it in the audit trail.</param>
        public void MarkCreationOrUpdate(Guid userId)
        {
            var now = DateTime.UtcNow;
            if (CreatedAt == DateTime.MinValue)
            {
                CreatedAt = now;
            }
            if (CreatedBy == Guid.Empty)
            {
                CreatedBy = userId;
            }
            Version++;
            LastUpdatedAt = now;
            LastUpdatedBy = userId;
        }

        /// <summary>
        /// Provided the user name of the current user, it sets Deleted values accordingly, using the UTC time.
        /// </summary>
        /// <param name="userId">ID of the user that is soft deleting the entity,
        /// to include it in the audit trail.</param>
        public void MarkDeletion(Guid userId)
        {
            if (IsDeleted && DeletedAt.HasValue && DeletedBy.HasValue) return;

            IsDeleted = true;
            DeletedAt = DateTime.UtcNow;
            DeletedBy = userId;
        }

        /// <summary>
        /// Copies the values of this <see cref="AuditInfo"/> instance to another.
        /// </summary>
        /// <param name="target">The instance of type <see cref="AuditInfo"/> where the values will
        /// be copied.</param>
        public void Copy(AuditInfo target)
        {
            target.Version = Version;
            target.ConcurrencyStamp = ConcurrencyStamp;
            target.CreatedAt = CreatedAt;
            target.CreatedBy = CreatedBy;
            target.LastUpdatedAt = LastUpdatedAt;
            target.LastUpdatedBy = LastUpdatedBy;
            target.IsDeleted = IsDeleted;
            target.DeletedAt = DeletedAt;
            target.DeletedBy = DeletedBy;
        }

        public AuditInfo Clone()
        {
            var newCopy = new AuditInfo();
            Copy(newCopy);
            return newCopy;
        }

        public void Reset()
        {
            Version = 0;
            ConcurrencyStamp = 0;
            CreatedAt = DateTime.MinValue;
            CreatedBy = Guid.Empty;
            LastUpdatedAt = DateTime.MinValue;
            LastUpdatedBy = Guid.Empty;
            IsDeleted = false;
            DeletedAt = null;
            DeletedBy = null;
        }

        public List<MemberComparison> CompareMembers(AuditInfo other)
        {
            return MemberComparer<AuditInfo>.Compare(other, this)
             .Property(p => p.Version)
             .Property(p => p.ConcurrencyStamp)
             .Property(p => p.CreatedAt)
             .Property(p => p.CreatedBy)
             .Property(p => p.LastUpdatedAt)
             .Property(p => p.LastUpdatedBy)
             .Property(p => p.IsDeleted)
             .Property(p => p.DeletedAt)
             .Property(p => p.DeletedBy)
             .GetDifferences();
        }

        public bool Equals(AuditInfo other)
        {
            return MemberComparer<AuditInfo>.CheckValueEquality(other, this)
             .Property(p => p.Version)
             .Property(p => p.ConcurrencyStamp)
             .Property(p => p.CreatedAt)
             .Property(p => p.CreatedBy)
             .Property(p => p.LastUpdatedAt)
             .Property(p => p.LastUpdatedBy)
             .Property(p => p.IsDeleted)
             .Property(p => p.DeletedAt)
             .Property(p => p.DeletedBy)
             .AreEqual();
        }

        public override int GetHashCode()
        {
            return new HashCodeCalculator<AuditInfo>(this)
             .Property(p => p.Version)
             .Property(p => p.ConcurrencyStamp)
             .Property(p => p.CreatedAt)
             .Property(p => p.CreatedBy)
             .Property(p => p.LastUpdatedAt)
             .Property(p => p.LastUpdatedBy)
             .Property(p => p.IsDeleted)
             .Property(p => p.DeletedAt)
             .Property(p => p.DeletedBy)
             .GetFinalHash();
        }
    }
}
