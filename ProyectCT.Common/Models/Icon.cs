﻿using System.Text.RegularExpressions;

namespace ProyectCT.Common
{
    /// <summary>
    /// Represents an icon (drawing or image) which can be used to identify visually an object.
    /// </summary>
    public struct Icon
    {
        /// <summary>
        /// Gets the name or ID of the icon library from which the current icon is taken.
        /// It is empty if the icon is an image defined by the <see cref="ImageUrl"/> property.
        /// </summary>
        public string IconLibrary { get; private set; }

        /// <summary>
        /// Gets the name or ID of the current icon, which should be unique in the context of the
        /// provided <see cref="IconLibrary"/>.
        /// It is empty if the icon is an image defined by the <see cref="ImageUrl"/> property.
        /// </summary>
        public string IconId { get; private set; }

        /// <summary>
        /// Gets the URL of the image used to represent this icon.
        /// </summary>
        public string ImageUrl { get; private set; }

        /// <summary>
        /// Gets <c>true</c> if this object is holding information of an icon or image; otherwise
        /// (if there's no icon or image data -all properties are empty-) then it's false.
        /// </summary>
        public bool IsSet { get; private set; }

        public Icon(string iconLibrary, string iconId)
        {
            IconLibrary = iconLibrary ?? "";
            IconId = iconId ?? "";
            ImageUrl = null;
            IsSet = true;
        }

        public Icon(string imageUrl)
        {
            IconLibrary = null;
            IconId = null;
            ImageUrl = imageUrl ?? "";
            IsSet = true;
        }

        public override string ToString()
        {
            if (ImageUrl != null)
                return $"url({ImageUrl})";

            if (IconId != null)
            {
                string library = string.IsNullOrWhiteSpace(IconLibrary) ? "" : IconLibrary + ", ";
                return $"icon({library}{IconId})";
            }

            return "none";
        }

        #region Static methods

        private static Regex _iconMatcher = new Regex(@"icon\(\s*(\'?(?<iconLibrary>[^\'\,\)]*)\'?\s*\,\s*)?\'?(?<iconId>[^\'\,\)]*)\'?\s*\)", RegexOptions.Compiled);
        private static Regex _urlMatcher = new Regex(@"url\(\s*\'?(?<url>[^\'\,\)]*)\'?\s*\)", RegexOptions.Compiled);

        public static Icon Parse(string iconString)
        {
            if (string.IsNullOrWhiteSpace(iconString))
                return new Icon();

            var lowerTrimmed = iconString.ToLower().Trim();
            if (lowerTrimmed == "none")
                return new Icon();

            var icon = _iconMatcher.Match(iconString);
            if (icon != null)
                return new Icon(icon.Groups["iconLibrary"].Value.Trim(), icon.Groups["iconId"].Value.Trim());

            var url = _urlMatcher.Match(iconString);
            if (url != null)
                return new Icon(url.Groups["url"].Value.Trim());

            return new Icon();
        }

        public static implicit operator Icon(string iconString)
        {
            return Parse(iconString);
        }

        public static implicit operator string(Icon icon)
        {
            return icon.ToString();
        }

        #endregion
    }
}
