﻿using Microsoft.Extensions.Configuration;


namespace ProyectCT.Common.Tests
{
    /// <summary>
    /// Simplifies the process of creation of DI container and configuration building
    /// for tests projects.
    /// </summary>
    public static class TestConfig
    {
        public static IConfigurationRoot GetIConfigurationRoot(string outputPath)
        {
            return new ConfigurationBuilder()
                .SetBasePath(outputPath)
                .AddJsonFile("appsettings.json", optional: true)
                .AddJsonFile("appsettings.Tests.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
