﻿namespace ProyectCT.Common
{
    /// <summary>
    /// This static class contains only constants useful for formatting of data and files.
    /// </summary>
    public static class FormatConstants
    {
        /// <summary>
        /// Format description of the ISO 8601 date (without time)
        /// </summary>
        public const string InvariantDateFormat = "yyyy-MM-dd";

        /// <summary>
        /// Format description of the ISO 8601 datetime (without timezone information)
        /// </summary>
        public const string InvariantDateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss";
    }
}
