﻿namespace ProyectCT.Common
{
    /// <summary>
    /// This static class contains only constants of RegEx patterns to be reused.
    /// </summary>
    public static class RegExConstants
    {
        /// <summary>
        /// This pattern is useful for validating names of technical components (i.e. IDs of 
        /// modules or extensions.
        /// </summary>
        public const string TechnicalIdentifiersPattern = @"[a-zA-Z0-9\-\._]+";

        /// <summary>
        /// This pattern is useful for validating the format of email addresses.
        /// </summary>
        public const string EmailPattern =
            @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
    }
}
