﻿namespace ProyectCT.Common
{
    /// <summary>
    /// This static class defines constants for common limits (as maximum lengths).
    /// </summary>
    public static class StandardLimits
    {
        /// <summary>
        /// Max length for the name of entities
        /// </summary>
        public const int RegularNameMaxLength = 100;

        /// <summary>
        /// Max length to be used in normal description/summary fields
        /// </summary>
        public const int RegularDescriptionMaxLength = 1000;

        /// <summary>
        /// Max length for the email addresses
        /// </summary>
        public const int EmailMaxLength = 254;

        /// <summary>
        /// Max length for file names (without complete path)
        /// </summary>
        public const int FileNameMaxLength = 255;

        /// <summary>
        /// Max length for the content types of files and media
        /// </summary>
        public const int ContentTypeMaxLength = 127;

        /// <summary>
        /// Max length for the URL addresses
        /// </summary>
        public const int UrlMaxLength = 1024;

        /// <summary>
        /// Max length for the UUIDs/GUIDs (if including brackets)
        /// </summary>
        public const int UuidMaxLength = 38;

        /// <summary>
        /// Max length for a password hash, considering that the password max length
        /// itself is of 32 characters.
        /// </summary>
        public const int PasswordHashMaxLength = 64;

        /// <summary>
        /// Max length for technical string identifiers
        /// </summary>
        public const int TechnicalIdentifier = 64;

        /// <summary>
        /// Max length for technical string references
        /// </summary>
        public const int TechnicalReference = 127;

    }
}
