﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace ProyectCT.Common.EntityFramework
{
    public static class EfExtensions
    {
        public static EntityTypeBuilder<T> AddAuditInfoAsOwned<T>(this EntityTypeBuilder<T> builder, string auditInfoPropertyName = "AuditInfo")
              where T : class
        {
            var auditInfoConfig = builder.OwnsOne(typeof(AuditInfo), auditInfoPropertyName);

            auditInfoConfig.Property("Version").HasColumnName("Version");
            auditInfoConfig.Property("ConcurrencyStamp").HasColumnName("ConcurrencyStamp").IsConcurrencyToken();
            auditInfoConfig.Property("CreatedAt").HasColumnName("CreatedAt");
            auditInfoConfig.Property("CreatedBy").HasColumnName("CreatedBy");
            auditInfoConfig.Property("LastUpdatedAt").HasColumnName("LastUpdatedAt");
            auditInfoConfig.Property("LastUpdatedBy").HasColumnName("LastUpdatedBy");
            auditInfoConfig.Property("IsDeleted").HasColumnName("IsDeleted");
            auditInfoConfig.Property("DeletedAt").HasColumnName("DeletedAt");
            auditInfoConfig.Property("DeletedBy").HasColumnName("DeletedBy");

            return builder;
        }

    }
}
