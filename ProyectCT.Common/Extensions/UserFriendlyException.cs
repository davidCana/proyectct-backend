﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Common.Extensions
{
    /// <summary>
    /// This class represents the exceptions that are describing the error in a user-friendly way (and most probably with
    /// localized strings), so they can be safely displayed to the users.
    /// </summary>
    public class UserFriendlyException : Exception
    {
        public UserFriendlyException() { }
        public UserFriendlyException(string message) : base(message) { }
        public UserFriendlyException(string message, Exception innerException) : base(message, innerException) { }
    }
}
