﻿using ProyectCT.Common.Extensions;
using ProyectCT.Common.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Common
{

    /// <summary>
    /// This exception class allows to provide details of more than one failed validation at once,
    /// having a collection of <see cref="ValidationResult"/> objects.
    /// <para>It inherits from <see cref="UserFriendlyException"/> as it is expected that all the
    /// messages reported by <see cref="ExtendedValidationException"/> are user friendly.</para>
    /// </summary>
    public class ExtendedValidationException : UserFriendlyException
    {
        public List<ValidationResult> ValidationErrors { get; private set; }

        public ExtendedValidationException(List<ValidationResult> validationErrors)
            : this(validationErrors.Count > 1
                    ? string.Format(CommonErrorMessages.ExistingValidationErrors, validationErrors.Count)
                    : validationErrors.FirstOrDefault()?.ErrorMessage,
                  validationErrors)
        {
        }

        public ExtendedValidationException(string message, List<ValidationResult> validationErrors) : base(message)
        {
            ValidationErrors = validationErrors;
        }

        public ExtendedValidationException(string message) : base(message)
        {
            ValidationErrors = new List<ValidationResult>() { new ValidationResult(message) };
        }

        public ExtendedValidationException(string message, params string[] memberNames) : base(message)
        {
            ValidationErrors = new List<ValidationResult>() { new ValidationResult(message, memberNames) };
        }
    }
}
