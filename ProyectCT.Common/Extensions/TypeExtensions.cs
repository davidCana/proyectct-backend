﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ProyectCT.Common
{
    /// <summary>
    /// Provides extension methods for handling types metadata.
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Checks if the type represented by a <c>Type</c> object is nullable/
        /// </summary>
        /// <param name="type"><see cref="System.Type"/> object with the type definition to evaluate</param>
        /// <returns><c>true</c> if the type is nullable. Otherwise, it returns <c>false</c></returns>
        public static bool IsNullableType(this Type type)
        {
            return (type.IsConstructedGenericType
                && type.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>
        /// Evaluates if a provided <see cref="Type"/> is a numeric primitive
        /// </summary>
        /// <param name="type">Type to be evaluated</param>
        /// <returns><c>true</c> if the type is <see cref="int"/>, <see cref="uint"/>, <see cref="short"/>, <see cref="ushort"/>,
        /// <see cref="byte"/>, <see cref="sbyte"/>, <see cref="float"/>, <see cref="double"/> or <see cref="decimal"/>.</returns>
        public static bool IsNumericType(this Type type)
        {
            if (IsNullableType(type))
            {
                type = type.GetGenericArguments()[0];
            }

            return type == typeof(int) || type == typeof(decimal) || type == typeof(float) || type == typeof(double) || type == typeof(uint)
                || type == typeof(short) || type == typeof(ushort) || type == typeof(byte) || type == typeof(sbyte);
        }

        /// <summary>
        /// Gets the localized name of the provided member property or field
        /// </summary>
        /// <param name="member">MemberInfo object with the metadata of the property or field</param>
        /// <returns>If the member has a DisplayAttribute assigned in its implementation (or in a base class), it returns the localized name according to the DisplayAttribute.
        /// If not, the interfaces will be checked to see if they implement the DisplayAttribute on a member of the same name.
        /// If no DisplayAttribute is found in the member for the class or any interface, then the same process is repeated but 
        /// with the Description attribute (non-localizable).
        /// If no Description is found either, the name of the member (in code) is returned.</returns>
        public static string GetDisplayName(this MemberInfo member)
        {
            var display = member.GetCustomAttributes<DisplayAttribute>(true).FirstOrDefault();
            if (display != null)
            {
                return display.GetName();
            }

            var objectInterfaces = member.DeclaringType.GetInterfaces();
            foreach (var @interface in objectInterfaces)
            {
                var interfaceMember = @interface.GetMember(member.Name).FirstOrDefault();
                if (interfaceMember == null) continue;
                display = interfaceMember.GetCustomAttributes<DisplayAttribute>(true).FirstOrDefault();
                if (display != null)
                {
                    return display.GetName();
                }
            }

            return member.Name;
        }

        /// <summary>
        /// Gets the localized short name of the provided member property or field
        /// </summary>
        /// <param name="member">MemberInfo object with the metadata of the property or field</param>
        /// <returns>If the member has a DisplayAttribute assigned in its implementation (or in a base class), it returns the localized short name according to the DisplayAttribute.
        /// If not, the interfaces will be checked to see if they implement the DisplayAttribute on a member of the same name.
        /// If no DisplayAttribute is found in the member for the class or any interface, then the same process is repeated but 
        /// with the Description attribute (non-localizable).
        /// If no Description is found either, the name of the member (in code) is returned.</returns>
        public static string GetDisplayShortName(this MemberInfo member)
        {
            var display = member.GetCustomAttributes<DisplayAttribute>(true).FirstOrDefault();
            if (display != null)
            {
                return display.GetShortName();
            }

            var objectInterfaces = member.DeclaringType.GetInterfaces();
            foreach (var @interface in objectInterfaces)
            {
                var interfaceMember = @interface.GetMember(member.Name).FirstOrDefault();
                if (interfaceMember == null) continue;
                display = interfaceMember.GetCustomAttributes<DisplayAttribute>(true).FirstOrDefault();
                if (display != null)
                {
                    return display.GetShortName();
                }
            }

            return member.Name;
        }
    }
}
