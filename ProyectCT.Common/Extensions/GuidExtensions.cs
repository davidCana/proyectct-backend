﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectCT.Common
{
    /// <summary>
    /// Provides extensions methods for <c>Guid</c> objects
    /// </summary>
    public static class GuidExtensions
    {
        /// <summary>
        /// Modifies a provided Guid to make it sequential, imitating the
        /// <c>NEWSEQUENTIALID</c> function in MS Sql Server.
        /// </summary>
        /// <param name="id">Guid to make sequential</param>
        /// <returns>Guid based on the provided <paramref name="id"/> which has been made sequential by 
        /// replacing its last 6 bytes with datetime-related values</returns>
        public static Guid AsSequential(this Guid id)
        {
            byte[] guidArray = id.ToByteArray();

            DateTime baseDate = new DateTime(1900, 1, 1);
            DateTime now = DateTime.Now;

            // Get the days and milliseconds which will be used to build the byte string 
            TimeSpan days = new TimeSpan(now.Ticks - baseDate.Ticks);
            TimeSpan msecs = now.TimeOfDay;

            // Convert to a byte array 
            // Note that SQL Server is accurate to 1/300th of a millisecond so we divide by 3.333333 
            byte[] daysArray = BitConverter.GetBytes(days.Days);
            byte[] msecsArray = BitConverter.GetBytes((long)(msecs.TotalMilliseconds / 3.333333));

            // Reverse the bytes to match SQL Servers ordering 
            Array.Reverse(daysArray);
            Array.Reverse(msecsArray);

            // Copy the bytes into the guid 
            Array.Copy(daysArray, daysArray.Length - 2, guidArray, guidArray.Length - 6, 2);
            Array.Copy(msecsArray, msecsArray.Length - 4, guidArray, guidArray.Length - 4, 4);

            return new Guid(guidArray);
        }
    }
}
